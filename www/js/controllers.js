angular.module('teles.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, $localStorage, $ionicPlatform, AuthFactory, messageFactory) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = $localStorage.getObject('userinfo','{}');
  $scope.registration = {};
  $scope.loggedIn = false;
  $scope.admin = false;

  if(AuthFactory.isAuthenticated()) {
      $scope.loggedIn = true;
      $scope.username = AuthFactory.getUsername();
      $scope.admin = AuthFactory.isAdmin();
  }

  // Login Modal
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.doLogin = function() {

    console.log('Doing login', $scope.loginData);
    $localStorage.storeObject('userinfo',$scope.loginData);
    AuthFactory.login($scope.loginData);
    $scope.closeLogin();

  };

  $scope.logOut = function() {
     AuthFactory.logout();
      $scope.loggedIn = false;
      $scope.username = '';
      $scope.admin = false;
  };
    
  $rootScope.$on('login:Successful', function () {
      $scope.loggedIn = AuthFactory.isAuthenticated();
      $scope.username = AuthFactory.getUsername();
      $scope.admin = AuthFactory.isAdmin();
  });

  // Register modal
  $ionicModal.fromTemplateUrl('templates/register.html', {
      scope: $scope
  }).then(function (modal) {
      $scope.registerform = modal;
  });

  $scope.closeRegister = function () {
      $scope.registerform.hide();
  };

  $scope.register = function () {
      $scope.registerform.show();
  };

  $scope.doRegister = function () {
      console.log('Doing registration', $scope.registration);
      $scope.loginData.username = $scope.registration.username;
      $scope.loginData.password = $scope.registration.password;

      AuthFactory.register($scope.registration);
      
      $scope.closeRegister();
  };
     
  $rootScope.$on('registration:Successful', function () {
      $scope.loggedIn = AuthFactory.isAuthenticated();
      $scope.username = AuthFactory.getUsername();
      $localStorage.storeObject('userinfo',$scope.loginData);
  });

  // View Message Modal
  $ionicModal.fromTemplateUrl('templates/messages.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.viewMessage = modal;
  });

  $scope.closeMessage = function() {
    $scope.viewMessage.hide();
  };

  $scope.openMessage = function() {
    $scope.viewMessage.show();
  };

  messageFactory.query(
    function (response) {
      $scope.msgs = response;
    },
    function (response) {}
  );

  $scope.deleteMsg = function(msgid){
    messageFactory.remove({id:msgid});
  };


})

.controller('EventCtrl', function($scope, eventFactory) {
  
  $scope.showEvent = false;

  eventFactory.query(
    function (response) {
      $scope.events = response;
      $scope.showEvent = true;
    },
    function (response) {}
  );

})

.controller('EventDetailCtrl', function($scope, $stateParams, eventFactory) {

  $scope.event = {};
  $scope.showEvent = false;

  $scope.event = eventFactory.get({
    id: $stateParams.id
  })
  .$promise.then(
    function (response) {
      $scope.event = response;
      $scope.showEvent = true;
    },
    function (response) {}
  );

})

.controller('PubCtrl', function($scope, pubFactory) {
  
  $scope.showPub = false;
  $scope.showDetail = false;

  $scope.toggleDetail = function () {
    $scope.showDetail = !$scope.showDetail;
  };

  pubFactory.query(
    function (response) {
      $scope.pubs = response;
      $scope.showPub = true;
    },
    function (response) {}
  );

})

.controller('PubDetailCtrl', function($scope, $stateParams, pubFactory) {

  $scope.pub = {};
  $scope.showPub = false;

  $scope.event = pubFactory.get({
    id: $stateParams.id
  })
  .$promise.then(
    function (response) {
      $scope.pub = response;
      $scope.showPub = true;
    },
    function (response) {}
  );

})

.controller('AboutController', function($scope, $ionicModal, boardFactory) {
  
  $scope.leaders = boardFactory.query();

})

.controller('ContactController', function($scope, messageFactory) {
  
  $scope.contact = {
    name: "",
    message: "",
    email: "",
    time: ""
  };

  // Message Modal
  $ionicModal.fromTemplateUrl('templates/contactform.html', {
    scope: $scope
  }).then(function (modal) {
    $scope.contactform = modal;
  });

  $scope.closeContact = function () {
    $scope.contactform.hide();
  };

  $scope.openContact = function () {
    $scope.contactform.show();
  };

  $scope.sendMessage = function () {
    $scope.contact.time = new Date();
    messageFactory.save($scope.contact);
    // Clear Message Form
    $scope.contact = {
      name: "",
      message: "",
      email: "",
      time: ""
    };
    $scope.closeContact();

  };

});
